---
layout: page
title: GIRLS 4 PRIVACY
permalink: /g4p/anuncio/
description: GIRLS 4 PRIVACY
image:
  feature: banners/header.jpg
timing: false
---

![g4p]({{ "/assets/images/g4p/2020/banner.jpg" }})

En **Interferencias** estamos comprometidas con las actividades que desarrollen la curiosidad y la evolución de diferentes colectivos por los derechos digitales, la privacidad y la tecnología. Creemos que aún estamos a tiempo de crear un futuro más justo en la red, de empoderar a los usuarios y potenciar la empatía digital. Pero no podemos hacerlo solas.

Desde que el grupo se creó en 2016, hemos estado detrás de charlas, eventos, hackathones, debates… Todo para **hacer ruido** y recordarle a los usuarios que tenemos derechos y debemos protegerlos. Poco a poco nos sumamos a más movimientos hermanos, y tenemos una firme postura en pro del software libre y el desarrollo digital. Hay un tema, dentro del contexto de la tecnología, que nos preocupa. Diversos estudios demuestran que hay un período clave entre la educación primaria y secundaria en la que las estudiantes (chicas) comienzan a perder interés en carreras técnicas por diversas razones, la mayoría estigmas culturales, pero también temor a un camino difícil, a una lucha constante por su valía como técnicas y a una considerable falta de modelos femeninos en la industria. Además el problema continúa, pues el porcentaje de abandono de las mujeres que deciden meterse en el mundo de la tecnología también es preocupante.

Hace unos meses le expusimos esta preocupación a [Kaspersky](https://www.kaspersky.es/), y decidieron ayudarnos. Queremos escuchar propuestas e ideas que nos ayuden a impulsar nuestros objetivos principales (la privacidad, los derechos digitales, la ciberseguridad….) y queremos hacerlo dandole voz a ese grupo de mujeres que están deseando aportar a la comunidad. Es por ello que, gracias al patrocinio de **Kaspersky Labs**, lanzamos una beca para sacar adelante un proyecto relacionado con los objetivos de Interferencias pero que esté compuesto en su mayoría (al menos del 60% del grupo) por mujeres. Además, la condición es que el grupo sea dirigido por una de éstas mujeres. Sabemos que habrá muy buenas ideas esperando a tener una oportunidad para desarrollarse, queremos formar parte de eso.

Lanzamos el concurso **Girls 4 Privacy**, una competición de proyectos con licencia libre en varias fases, con la idea de poder becar la propuesta ganadora.

Desde los inicios del grupo, hemos defendido la importancia de la multidisciplinaridad en todos nuestros proyectos y es por eso que animamos a todo tipo de perfiles a apuntarse en esta competición. ¿Eres periodista? ¿Trabajas en educación? ¿Artista quizás? ¿Estás en el mundo de la ingeniería? Nos encantaría escuchar cualquier idea de cualquier disciplina. Es más, animamos a ello.

**Las bases completas del concurso podéis encontrarlas [aquí]({{ "/assets/pdfs/CONVOCATORIA_BECA_GIRLS4PRIVACY.pdf" }})**, pero en cualquier vamos a hacer un pequeño resumen:

- El grupo debe estar conformado de dos a cinco personas (consultar excepciones con organización).
- El grupo debe estar conformado en al menos un 60% por mujeres. La edad y otros factores son irrelevantes.
- El proyecto debe tener licencia libre con copyleft aplicada a todo el proyecto (hay mucha más información sobre esto en las [bases]({{ "/assets/pdfs/CONVOCATORIA_BECA_GIRLS4PRIVACY.pdf" }})).
- La temática debe estar relacionada con privacidad, derechos digitales y ciberseguridad.
- El plazo de presentación de propuestas es hasta el 15 de diciembre a través de [g4p@interferencias.tech](mailto:g4p@interferencias.tech).

### ¿Cuáles son las fases?

- Presentación de la idea de proyecto: ¿Cuáles son los objetivos? ¿De qué va vuestro proyecto? ¿Quiénes sois?
- Desarrollo y avance del proyecto: se valorará la organización, el trabajo en equipo, la funcionalidad y la originalidad mediante tres pequeños informes en las fechas que se indican en las [bases]({{ "/assets/pdfs/CONVOCATORIA_BECA_GIRLS4PRIVACY.pdf" }}).
- Selección de proyecto ganador, seguimiento de su evolución y apoyo económico y mediático.

### ¿Cuál es el premio?

El premio son **700€**, importe que deberá destinarse al desarrollo del proyecto más allá de la finalización del certamen. Consideramos que puede ser un empuje importante para una idea que, como todas, necesitará adquirir medios y recursos que faciliten llevar a cabo todo tipo de investigaciones y acciones. Los fondos para la financiación de esta beca han sido provistos por **Kaspersky Lab**.

- [Convocatoria beca “GIRLS 4 PRIVACY”]({{ "/assets/pdfs/CONVOCATORIA_BECA_GIRLS4PRIVACY.pdf" }})

<div class="bootstrap">
  <div class="text-center">
  <br>
  <h3>Estamos deseando escuchar todas vuestras propuestas!!!</h3>
  <hr>
</div>
</div>
