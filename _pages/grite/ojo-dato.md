---
layout: page
title: Charlas con GRITE
permalink: /charlas-grite/
description: Charlas con GRITE
image:
  feature: banners/header.jpg
timing: false
---

<img src="{{ site.url }}/assets/images/grite.jpg" style="display: block; margin: 0 auto;">

EL **GRITE** es un *Grupo de Reﬂexión y de Información sobre las Tecnológicas Educativas* formado por miembros de la
**AMPA del Gómez Moreno** y abierto al conjunto de la comunidad educativa. Colabora con **Interferencias** en la defensa de la privacidad y los derechos digitales, así como reivindicar la necesidad del uso del software libre.

*Puedes encontrar más información sobre el AMPA del Gómez Moreno y el GRITE en su <a href="https://ampagomezmoreno.wordpress.com/" target="_blank">web</a>.*

<h1> Próxima charla </h1>
<p style="text-align:center">
    <img src="/assets/images/grite/2021-05-04.jpg">
</p>

<h1> Charlas anteriores </h1>

<div class="row">
  <div class="column_photo">
    <img src="https://avatars.githubusercontent.com/u/500?v=4">
  </div>
  <div class="column_text">
    <h2 style="margin-top: 8px !important;"><a href="https://twitter.com/jjmerelo" target="_blank">JJ Merelo</a>: "Ojo al dato: qué pasa cuando estamos en internet, a quién le interesa, y qué podemos hacer al respecto"</h2>
  </div>
</div>

<p style="text-align:center">
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tubedu.org/videos/embed/54cb958d-b098-4df9-84cd-c8f88cb7db2f" frameborder="0" allowfullscreen></iframe>
</p>

<div class="row">
  <div class="column_photo">
    <img src="/assets/images/authors/terceranexus6.jpg">
  </div>
  <div class="column_text">
    <h2 style="margin-top: 8px !important;"><a href="https://twitter.com/Terceranexus6" target="_blank">Paula de la Hoz</a>: "¿Qué pasa con WhatsApp?"</h2>
  </div>
</div>

<p style="text-align:center">
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tubedu.org/videos/embed/c2609af0-bcac-454e-a7a3-baede8a4f908" frameborder="0" allowfullscreen></iframe>
</p>
