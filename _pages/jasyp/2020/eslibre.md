---
layout: page
title: Sala esLibre 2020&#58 Derechos Digitales, Privacidad en Internet y Seguridad Informática
permalink: /eslibre/2020/
description: "Propuesta sala esLibre 2020"
image:
  feature: banners/header.jpg
timing: false
---

<img src="{{ site.url }}/assets/images/eslibre/eslibre_2020.jpg" style="display: block; margin: 0 auto;">

<p>
Desde <strong>Interferencias</strong> siempre nos gusta colaborar en iniciativas que nos parecen interesantes, y es por eso que tanto el <a href="https://eslib.re/2019/" target="_blank">año pasado</a> como <a href="https://eslib.re/2020/" target="_blank">este año</a> estamos colaborando en la organización de <strong>es<span style="color:red">Libre</span></strong>: un encuentro de personas interesadas en el conocimiento y la difusión del las <em>tecnologías libres y cultura abierta</em>. Concretamente <strong>la edición de este año será totalmente online los días 18 y 19 de septiembre</strong>, coincidiendo con el <strong>"<a href="https://www.softwarefreedomday.org/" target="_blank">Software Freedom Day</a>"</strong> (<em>que además, también es el cumpleaños oficioso de Interferencias</em> 🌚).
</p>

<p>
Como ya sabréis, este año finalmente no pudimos realizar nuestras <strong><a href="{{ site.url }}/jasyp" target="_blank">JASYP '20</a></strong> debido a la situación actual, y ya que todos los años planteamos las Jornadas como una celebración donde compartir experiencias y conocimiento con la espontaneidad del trato en persona decidimos que no tendría sentido celebrarlas de forma online porque perdería su significado; <em>no se trata de ser otro congreso, se trata de ser una experiencia aprovechando todo lo que nos brinda Granada</em>.
</p>

<p>
Por todo esto que comentamos, finalmente hemos decidido presentar una <strong>propuesta de sala</strong> dentro del programa de <strong>es<span style="color:red">Libre</span></strong> para seguir llegando a tanta gente como sea posible. En un momento en el que las grandes compañías tecnológicas se frotan las manos pensando en todo el <em>beneficio que podrán sacar de nuestros datos</em> o el peligro que puede entrañar la tecnología más inocente del mundo si se le da usos con objetivos cuestionables (o peor, inconscientes), es más necesario que nunca disponer del conocimiento sobre cómo funcionan las tecnologías que está tan integradas en nuestro día a día que nos rodean de forma casi inapreciable.
</p>

<blockquote>
<strong>Hoy más que nunca debemos ser conscientes de la necesidad de proteger nuestra privacidad y nuestra soberanía digital</strong>: tenemos que saber <strong>QUÉ</strong> tenemos a nuestra disposición y <strong>CÓMO</strong> podemos actuar; y esto, sin duda alguna, es algo que viene de la mano del <strong>Software Libre</strong>.
</blockquote>

<p>
El planteamiento del contenido será muy parecido al que hacemos en las JASYP, pero con un formato algo distinto:
</p>

<h2 style="text-align: center;" id="programa">esLibre 2020: "Derechos Digitales, Privacidad en Internet y Seguridad Informática"</h2>

<h3 style="text-align: center;"><a href="https://eslib.re/2020/programa/" target="_blank">Programa completo de es<span style="color:red">Libre</span> 2020</a> y todos los vídeos de las actividades en <a href="https://commons.wikimedia.org/wiki/Category:EsLibre_2020" target="_blank">Wikimedia Commons</a></h3>

<h3 style="text-align: center;">Viernes 18 de septiembre</h3>

<h3 id="inicio">12:30-12:40 Presentación 'Sala Derechos Digitales, Privacidad en Internet y Seguridad Informática'</h3>

**Germán Martínez**: Ingeniero informático, activista por los derechos digitales y el software libre, coordinador en **Interferencias** y <a href="https://librelabgrx.cc/" target="_blank"><strong>LibreLabGRX</strong></a> y parte del equipo de organización de <a href="https://eslib.re/" target="_blank"><strong>esLibre</strong></a>.

* Puedes encontrarle en Mastodon como <a href="https://mastodon.social/@germaaan_/" target="_blank">@germaaan_@mastodon.social</a> y en Twitter como <a href="https://twitter.com/germaaan_" target="_blank">@germaaan_</a>.

* Enlace al vídeo en <a href="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.00_-_Germ%C3%A1n_Mart%C3%ADnez_-_Presentaci%C3%B3n_Sala_Derechos_Digitales,_Privacidad_en_Internet_y_Seguridad_Inform%C3%A1tica.webm" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.00_-_Germ%C3%A1n_Mart%C3%ADnez_-_Presentaci%C3%B3n_Sala_Derechos_Digitales,_Privacidad_en_Internet_y_Seguridad_Inform%C3%A1tica.webm?embedplayer=yes" width="512" height="288" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h3 id="primera">12:40-13:10 Herramientas para mantener tu privacidad en Internet</h3>

**Pablo Castro**: activista del software libre y la libertad en Internet. Forma parte de la asociación <a href="https://trackula.org/" target="_blank">**Trackula**</a> y se encarga de <a href="https://www.techtopias.com/" target="_blank">**Techtopias**</a>, una newsletter quincenal sobre privacidad, soberanía digital y libertad en internet para estar al día de las noticias mas interesantes de este mundillo.

* Puedes encontrarle en Mastodon como <a href="https://mastodon.social/@castrinho8/" target="_blank">@castrinho8@mastodon.social</a> y en Twitter como <a href="https://twitter.com/castrinho18" target="_blank">@castrinho18</a>.

<blockquote>
En pleno 2020, muchas grandes empresas tecnológicas basan su modelo de negocio en vender nuestra vida privada, nuestra forma de ser, qué vamos a ser en el futuro, llegando incluso a manipularnos.
<br><br>En esta charla veremos rápidamente los peligros a los que se enfrenta la sociedad tecnológica actual y qué herramientas podemos tener e instalar de forma sencilla para intentar reducir la información que generamos a diario y así luchar por preservar nuestra privacidad.
</blockquote>

* Enlace al vídeo en <a href="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.01_-_Pablo_Castro_-_Herramientas_para_mantener_tu_privacidad_en_Internet.webm" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.01_-_Pablo_Castro_-_Herramientas_para_mantener_tu_privacidad_en_Internet.webm?embedplayer=yes" width="512" height="288" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h3 id="segunda">13:10-13:45 Self Sovereign Identity: identidad bajo el control de las personas</h3>

**Jorge Cuadrado**: investigador en ciberseguridad en el laboratorio de innovación de **BBVA Next Technologies**. Estudió Ingeniería Informática en la Universidad de Valladolid (UVA) y el Máster en Ciberseguridad de la Universidad Carlos III de Madrid (UC3M). Ha participado como ponente en múltiples congresos como: Cybercamp, RootedCon, Hack In The Box, X1RedMásSegura o **JASYP**. Sus focos de interés, sobre los que siempre tiene algún proyecto en marcha, son: privacidad, impresión 3D y electrónica.

* Puedes encontrarle en Twitter como <a href="https://twitter.com/Coke727" target="_blank">@coke727</a> y su web personal <a href="https://www.croke.es" target="_blank">https://www.croke.es</a>.

<blockquote>
Self Sovereign Identity (SSID) es un sistema de identificación que ha surgido recientemente y que, conceptualmente, cambia significativamente respecto a modelos más tradicionales de identificación.
<br><br>Mediante este esquema, las personas adquieren la responsabilidad de gestionar cómo y en qué cantidad se usan sus datos personales. Para ello, permite crear “tarjetas de identidad” digitales que pueden ser generales para cualquier servicio o específicas para cada caso. Pero… ¿qué ventajas y peligros hay detrás de SSID?
</blockquote>

* Enlace al vídeo en <a href="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.02_-_Jorge_Cuadrado_-_Self_Sovereign_Identity,_identidad_bajo_el_control_de_las_personas.webm" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.02_-_Jorge_Cuadrado_-_Self_Sovereign_Identity,_identidad_bajo_el_control_de_las_personas.webm?embedplayer=yes" width="512" height="288" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h3 id="tercera">16:00-16:25 ¿Existe vacuna para los sesgos en las inteligencias artificiales?</h3>

**Emma Lopez**:

* Puedes encontrarle en Twitter como <a href="https://twitter.com/hell03610/" target="_blank">@hell03610</a>.

<blockquote>
Los humanos tenemos sesgos y parece que las máquinas inteligentes también, pero, ¿se trata esto de una feature o de un bug?
<br><br>En esta charla veremos qué son las llamadas inteligencias artificiales y qué son los sesgos, por qué es tal fácil que permeen en el software, cuál es su impacto y qué podemos hacer para detectarlos y mitigarlos.
</blockquote>

* Enlace al vídeo en <a href="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.03_-_Emma_L%C3%B3pez_-_%C2%BFExiste_vacuna_para_los_sesgos_en_las_inteligencias_artificiales%3F.webm" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.03_-_Emma_L%C3%B3pez_-_%C2%BFExiste_vacuna_para_los_sesgos_en_las_inteligencias_artificiales%3F.webm?embedplayer=yes" width="512" height="288" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h3 id="cuarta">16:25-16:40 Fact-checking y fake news: caja de herramientas ciudadana para la verificación</h3>

**Javier Cantón**: sociólogo y comunicador audiovisual. Doctor en Ciencias Sociales, ha impartido docencia en las Universidades de Jaén, Granada, UNIR, Girona y UOC, así como diversos talleres y colaboraciones sobre pensamiento visual y visualización de datos. Investiga sobre desigualdad, redes sociales, visualidad y, más recientemente, desinformación y fake news, tema sobre el que ha elaborado material docente para una asignatura universitaria. Actualmente trabaja en <a href="https://medialab.ugr.es" target="_blank"><strong>Medialab UGR</strong></a>, coordinando <a href="https://medialab.ugr.es/proyectos/radiolab-ugr/" target="_blank"><strong>RadioLab UGR</strong></a> entre otros proyectos, e imparte docencia en UNIR y UOC.

* Puedes encontrarle en Twitter como <a href="https://twitter.com/ProsumidorSoc" target="_blank">@ProsumidorSoc</a> y en su página web personal <a href="javiercanton.com" target="_blank">javiercanton.com</a>.

<blockquote>
Saturados de información y desconfianza, resulta cada vez más complicado discernir la información cierta de la falsa. La proliferación de las denominadas fake news está reconfigurando la esfera pública y los cauces de formación de la opinión pública, lo que supone un reto para nuestras democracias, que se fundamentan en el acceso a una información veraz para sus ciudadanos.
<br><br>En este taller aprenderás diferentes métodos y herramientas libres para verificar la procedencia de una noticia y luchar contra las fake news y los bulos.
</blockquote>

* Enlace al vídeo en <a href="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.04_-_Javier_Cant%C3%B3n_-_Fact-checking_y_fake_news,_caja_de_herramientas_ciudadana_para_la_verificaci%C3%B3n.webm" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.04_-_Javier_Cant%C3%B3n_-_Fact-checking_y_fake_news,_caja_de_herramientas_ciudadana_para_la_verificaci%C3%B3n.webm?embedplayer=yes" width="512" height="288" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h3 id="quinta">16:40-17:15 Social Justice Code Repositories</h3>

**Lorena Sánchez**: Consultora de seguridad y cumplimiento en **ElevenPaths**. Jurista y politóloga por la Universidad Carlos III de Madrid. Máster en Gobierno de las Telecomunicaciones por la London School of Economics. Especialista en Derecho Tecnológico e Informática Forense. Editora del medio de análisis político y social <a href="https://polikracia.com/" target="_blank"><strong>Polikracia</strong></a>. Ha centrado su carrera profesional en la seguridad en organizaciones, aunque está especializada en derecho tecnológico, protección de datos, seguridad informática y su relación con las ciencias sociales y el derecho. En este sentido, investiga la interrelación entre política y tecnología desde distintos planos como los derechos fundamentales, la privacidad o las relaciones internacionales. Algunos de sus proyectos versan sobre infraestructuras críticas y seguridad internacional; seguridad y privacidad en sistemas de voto electrónico o la concepción de la privacidad en el plano social.

* Puedes encontrarle en Twitter como <a href="https://twitter.com/LastStrawberry" target="_blank">@LastStrawberry</a>.

<blockquote>
¿Garantiza el software libre que el usuario sea libre? ¿Son necesarios y suficientes los principios del FSM para garantizar la justicia? ¿Cuál es el rol de los repositorios de código en la promoción de la justicia en el software? Estas son algunas de las preguntas que intentaremos desgranar en la charla con el propósito principal de analizar bajo qué condiciones el Free Software Movement funciona para alcanzar la justicia en la producción, uso y consumo de software. Así, la base de la charla será retar el funcionamiento de las tesis de Richard Stallman como teoría, poniendo en valor el rol de los repositorios de código abierto y buscando los puntos débiles que deben ser mejorados.
<br><br>Si bien la charla presenta un punto de vista teórico, pretende ponerle sentido al FSM a través de diferentes teorías de la justicia preguntándose cómo el FSM puede ser clave en la libertad del usuario sin quedarse en papel mojado.
</blockquote>

* Enlace al vídeo en <a href="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.05_-_Lorena_S%C3%A1nchez_-_Social_Justice_Code_Repositories.webm" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.05_-_Lorena_S%C3%A1nchez_-_Social_Justice_Code_Repositories.webm?embedplayer=yes" width="512" height="288" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h3 id="sexta">17:30-17:55 Software Libre para las personas</h3>

**Paula de la Hoz**: Co-fundadora de **Interferencias**. Senior Red Team en **Telefónica Ingeniería de Seguridad**.

* Puedes encontrarle en Mastodon como <a href="https://cybre.space/@terceranexus6" target="_blank">@terceranexus6@cybre.space</a> y en Twitter como <a href="https://twitter.com/Terceranexus6" target="_blank">@Terceranexus6</a>.

<blockquote>
Desde su origen, el Software Libre está ligado a la comunidad, a ser partícipe de proyectos con el ánimo de que lleguen a otras manos, las que sean, y sigan mejorando.
<br><br>En la actualidad, desde asociaciones hasta grupos de vecinos se enfrentan a diario a un mundo ya protagonizado por la tecnología, y es aquí donde el software libre debería ser una herramienta esencial que defienda los intereses de las personas sin que ello suponga ceder seguridad y privacidad.
<br><br>En esta charla pretendo presentar las posibles soluciones libres para las necesidades de asociaciones y otras comunidades, entre ellas <a href="https://gitlab.com/terceranexus6/barriocheck" target="_blank">BarrioCheck</a>, un proyecto para alertas de barrios basado en OSM que desarrollé hace poco y que pretende dar comienzo en Madrid.
<br><br>Durante la charla pretenden cubrirse los siguientes temas: comunicación segura y libre, hardening básico e introducción a sysadmin de comunidades, instrucciones y recursos para nivel de usuario para miembros no técnicos, BarrioCheck, hardware y software libre para centros/bibliotecas/radios sociales; entre otros.
</blockquote>

* Enlace al vídeo en <a href="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.06_-_Paula_de_la_Hoz_-_Software_Libre_para_las_personas.webm" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.06_-_Paula_de_la_Hoz_-_Software_Libre_para_las_personas.webm?embedplayer=yes" width="512" height="288" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h3 id="septima">17:55-18:20 Hacktivismo no es nombre de señora</h3>

**Sofía Prósper**: Defensora de la privacidad, arquitecta e ilustradora. Forma parte de la asociación <a href="https://trackula.org/" target="_blank">**Trackula**</a> y actualmente se encuentra construyendo <a href="https://iuvia.io" target="_blank">**IUVIA**</a>, ambos proyectos centrados en la defensa de la privacidad en Internet.

* Puedes encontrarle en Twitter como <a href="https://twitter.com/sofipros" target="_blank">@sofipros</a>.

<blockquote>
Viviendo el año más extraño de nuestras vidas, este atípico 2020 nos deja con nuestros derechos digitales afectados y con unos precedentes en solucionismo tecnológico que nadie hubiera imaginado si una pandemia no los hubiera instaurado velozmente.
<br><br>El capitalismo de la vigilancia es sólo un síntoma de un sistema económico y político funcionando como se espera. Por eso es el momento idóneo para movernos, para investigar, para estar informadas y no parar de defender nuestras libertades antes de que la tecnología ‘nos venga a salvar’.
<br><br>En esta charla nos gustaría, a parte de hacer una pequeña introducción de cuál es el estado actual de las cosas, mostrar proyectos e iniciativas que no se han rendido al ‘yo no tengo nada que esconder’ y siguen al pie del cañón llegando cada día a más gente.
</blockquote>

* Enlace al vídeo en <a href="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.07_-_Sof%C3%ADa_Pr%C3%B3sper_-_Hacktivismo_no_es_nombre_de_se%C3%B1ora.webm" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.07_-_Sof%C3%ADa_Pr%C3%B3sper_-_Hacktivismo_no_es_nombre_de_se%C3%B1ora.webm?embedplayer=yes" width="512" height="288" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h3 id="octava">18:20-18:45 ¿Hacia un feudalismo de los datos?¿Derecho o tecnología?¿Cómo defendernos?</h3>

**Luis Fajardo**: de perfil claramente universitario, ha sido juez y abogado. Fue responsable de las redes de la Facultad de Derecho de la UAM en los años 90, siendo becario de Formación de Profesorado Universitaario (FPI/FPU). Ha impartido docencia en las Universidades Autónoma de Madrid, Almería, Gerona, UNED y La Laguna, donde lo sigue haciendo en la actualidad, adscrito al Área de Derecho civil. Ha sido fundador de varias aociaciones, entre ellas el **Centro de Alternativas Legales**, y miembro de **Madrid Wireless**, el <a href="https://www.gulic.org/" target="_blank">**Grupo de Usuario Linux de Canarias (GULiC)**</a>, Secretario de ESLIC (Empresas de Software Libre de Canarias), y la <a href="https://www.ull.es/servicios/osl/" target="_blank">**Oficina del Software Libre de la ULL**</a>, con la que viene colaborando desde hace muchos años. Desde 1999 usa exclusivamente GNU/Linux en sus sistemas. Mantiene y administra diversos nodos del Fediverso, como <a href="https://encanarias.info/" target="_blank">https://encanarias.info</a> (**Diaspora**) y <a href="https://silba.me/" target="_blank">**Silba.Me**</a> (**Mastodon**).

* Puedes encontrarle en Diaspora desde <a href="https://encanarias.info/people/56066f30d1620135a1394b44294f2d2e" target="_blank"> encanarias.info</a>, en Mastodon como <a href="https://silba.me/@lfajardo" target="_blank">@lfajardo@silba.me</a> y en Twitter como <a href="https://twitter.com/lfajardo" target="_blank">@lfajardo</a>.

<blockquote>
La Administración está llena de malas prácticas, el mercado está dirigido a primar la corporocracia, que está sustituyendo a la democracia.
<br><br>El Fediverso, las redes federadas, y los servicios libres (que pueden ser instalados “on premise”), pero no parecen suficientes. Realmente tenemos normas que intentan limitar el avance de la corporocracia, fundamentalmente el RGPD. Lamentablemente no se está usando, y debe lanzarse un aviso a navegantes.
<ul>
<li>Cómo nos enganchan: dispositivos que no obedecen a sus dueños, redes sociales que polarizan, manipulación informativa e información individualizada…</li>
<li>Cómo defenderse: elección de los servicios con unos parámetros técnicos (arranque libre, open source, interoperable, estándar y federado…) y jurídicos (que permite exigir aquellos criterios técnicos, básicamente, RGPD).</li>
</ul>
</blockquote>

* Enlace al vídeo en <a href="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.08_-_Luis_Fajardo_-_%C2%BFHacia_un_feudalismo_de_los_datos%3F%C2%BFDerecho_o_tecnolog%C3%ADa%3F%C2%BFC%C3%B3mo_defendernos%3F.webm" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre2020_18V.SDD.08_-_Luis_Fajardo_-_%C2%BFHacia_un_feudalismo_de_los_datos%3F%C2%BFDerecho_o_tecnolog%C3%ADa%3F%C2%BFC%C3%B3mo_defendernos%3F.webm?embedplayer=yes" width="512" height="288" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h1 style="text-align: center;">#somosruido 💻✊📣</h1>

<figure>
 <img src="{{ "/assets/images/jasyp/18/mosaico.jpg" }}" alt="Mosaico JASYP '17" style="width:100%">
 <figcaption style="text-align: center; padding-top: 0em"><strong>JASYP '17</strong></figcaption>
</figure>

<figure>
 <img src="{{ "/assets/images/jasyp/18/charlas/mosaico_v.jpg" }}" alt="Mosaico JASYP '18" style="width:100%">
 <figcaption style="text-align: center; padding-top: 0em"><strong>JASYP '18</strong></figcaption>
</figure>

<figure>
 <img src="{{ "/assets/images/jasyp/20/mosaico_jasyp_2019_small.png" }}" alt="Mosaico JASYP '19" style="width:100%">
 <figcaption style="text-align: center; padding-top: 0em"><strong>JASYP '19</strong></figcaption>
</figure>
