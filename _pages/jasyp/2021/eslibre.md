---
layout: page
title: Sala esLibre 2021&#58 Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google)
permalink: /eslibre/2021/
description: "Propuesta Sala Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google) en esLibre 2021"
image:
  feature: banners/header.jpg
timing: false
---

<blockquote>
Se ha demostrado en los últimos meses que la colaboración, la autogestión y las comunidades constructivas son la clave para hacer funcionar el mundo frente a toda corriente. En esta situación, el software y el hardware libre tienen un hueco especial, conformado por una comunidad multidisciplinar dispuesta a colaborar por avance tecnológico responsable.<br><br><strong>La comunidad de software/hardware libre ahora más que nunca debe estar dispuesta a crear herramientas prácticas que ayuden a la sociedad a avanzar de manera transparente y segura</strong>.
</blockquote>

Desde **Interferencias apoyamos firmemente el software libre**, creemos necesaria una tecnología responsable para incidir en varios elementos clave de la sociedad, además de la transparencia, la privacidad y los derechos digitales como base de cualquier proyecto. Uno de nuestros focos principales está en la educación, cuyas competencias tecnológicas se han acelerado en los últimos meses hacia recursos que poco cuidan la transparencia de su metodología. Romper la cadena de las herramientas privativas es una de las tareas más complicadas, pues se aprovechan de las dinámicas rápidas y fugaces en las que vivimos para obviar los muchos problemas que esconde el uso indebido de datos. Pero tenemos esperanza.

<img src="{{ site.url }}/assets/images/eslibre/2021/eslibre_2021_small.png" style="display: block; margin: 0 auto;">

<p>
Por eso, otro año más organizamos dentro de <a href="https://eslib.re/2021/programa/" target="_blank"><strong>es<span style="color:red">Libre</span></strong></a> una sala centrada en estas temáticas de Derechos Digitales y Privacidad en Internet, tratando además de forma específica el problema que estamos viendo con la soberanía digital en las aulas de colegios e institutos, donde a un ritmo acelerado se está haciendo una implantación masiva de software no respetuoso con la privacidad con la excusa de la situación sanitaria actual.
</p>

Para esto último hemos contactado con diferentes colectivos que están intentando promover alternativas a los acuerdos que se están llegando desde las diferencias Administraciones, entregando de forma totalmente irresponsable la gestión de la educación a empresas como Google o Microsoft, empresas con una total falta de ética en lo que corresponde al respeto de la privacidad.

<h2 style="text-align: center;" id="programa"><a href="https://eslib.re/2021/programa/" target="_blank"><strong>es<span style="color:red">Libre</span></strong> 2021</a>: "Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google)"</h2>

<h3 style="text-align: center;">Viernes 25 de junio</h3>

<h2 style="text-align:center">BLOQUE A: "Soberanía Digital en las Aulas (Fuera Google)"</h2>

<h2 id="digitalizacion-democratica-centros-educativos">11:00-11:25 "Excelencia en la privacidad de datos y la digitalización democrática de los centros educativos"</h2>

**Sergio Salgado** estudió Ciencias Políticas en la Universidade de Santiago de Compostela y Ecología Humana en la Universidade Aberta de Lisboa. Fundador de Pantheon Work, consultora especializada en la gestión de comunicación, organización y reputación. Asesora varias organizaciones e instituciones. Está Implicado en varios proyectos de activismo y es coautor junto con Simona Levi del Libro «Votar y cobrar – La impunidad como forma de gobierno» y de la obra teatral «Hazte Banquero».

* **Web del proyecto**: <https://xnet-x.net/privacidad-datos-digitalizacion-democratica-educacion-sin-google/>
* **Web personal**: <https://xnet-x.net/sergio-salgado/>
* **Twitter**: [@X_net_](https://twitter.com/X_net_)

<blockquote>
Propuesta de solución de Xnet y familias promotoras para la excelencia en la privacidad de datos y la digitalización democrática de los centros educativos.<br><br>
Con la intención de paliar el déficit existente sobre privacidad y soberanía de datos en los centros educativos proponemos un Plan holístico que nos facilitará acceder a una digitalización democrática ágil, efectiva y respetuosa de las libertades fundamentales.<br><br>
Objetivos específicos a alcanzar para resolver el problema global:<br><br>
1. Servidores seguros y respetuosos con los derechos humanos y acciones relacionadas<br><br>
2. Suite for Education: herramientas estructuralmente auditables que ya existen agregadas en una única suite<br><br>
3. Formación y capacitación digital de la comunidad educativa: task force para traspaso de las competencias y adecuación de los programas de formación digital<br><br>
4. Cuestiones transversales para ayudar el ecosistema empresarial, el correo y otras cuestiones relacionadas con la digitalización general<br><br>
5. Diseño de implementación y presupuesto de nuestro plan de digitalización democrática de los centros educativos<br><br>
6. Repositorio de información para argumentarios<br><br>
</blockquote>

* Enlace al vídeo en <a href="https://w.wiki/4Jrg" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre_2021_P24_-_Sergio_Salgado_-_Excelencia_en_la_privacidad_de_datos_y_la_digitalizaci%C3%B3n_democr%C3%A1tica_de_los_centros_educativos.webm?embedplayer=yes" width="640" height="360" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h2 id="familias-criticas-opensource">11:25-11:50 "Familias críticas por el Open Source"</h2>

El **GRITE** es un **Grupo de Reflexión y de Información sobre las Tecnologías Educativas** formado por miembros de la **AMPA del CEIP Gómez Moreno (Granada)** y abierto al conjunto de la comunidad educativa. Nuestra propuesta quiere activar la colaboración con el centro para argumentar y reflexionar en base a la realidad de nuestra comunidad educativa. Participarán en su representación Marion, Manu y Rakel.

* **Twitter**: [@ampagomezmoreno](https://twitter.com/ampagomezmoreno)
* **PeerTube**: <https://tubedu.org/video-channels/grite_gomez/videos>
* **Correo electrónico**: <grite_gomez@protonmail.com>

<blockquote>
Buscamos realizar un diagnóstico participativo. Para ello, necesitaríamos cierta información para trabajar juntas en el óptimo desarrollo crítico de las competencias digitales. Por eso es imprescindible cuantificar la situación, por ejemplo, conocer cuantas familias han dado su consentimiento sobre las políticas de tratamiento de datos de la plataforma Google for Education que se han empezado a usar masivamente en los centros educativos, así como investigar el estado del conocimiento digital de las familias (digital readiness) y, por tanto, el grado de consentimiento informado que se ha dado en los datos anteriormente solicitados.<br><br>
Hemos estado trabajando en el diseño de unos objetivos que motivan esta iniciativa y queremos compartirlo con vosotras y vosotros para trabajar toda la comunidad educativa alineada:<br><br>
- Divulgar y sensibilizar sobre el programa de Transformación Digital Educativa (TDE) de la Junta de Andalucía, acompañando el proceso de implementación desde una perspectiva que garantice la privacidad, intimidad y seguridad de lxs menores y promueva la autonomía, la autogestión y la soberanía digital sobre los datos. <br><br>
- Reflexionar sobre el uso de GAFAM en primaria y posibles alternativas.<br><br>
- Apoyar la utilización del software libre.<br><br>
- Privilegiar soluciones no tecnológicas y presenciales cuando es posible.<br><br>
- Formación multidisciplinar y transversal sobre el entorno digital.<br><br>
- Fomentar la capacidad de los jóvenes de utilizar las nuevas tecnologías de forma segura y responsable.<br><br>
- Que las nuevas tecnologías sean un instrumento de aprendizaje para el alumnado y que este no sea instrumento de esas nuevas tecnologías.<br><br>
- Promover el desarrollo sostenible (green IT) en el uso de las nuevas tecnologías: circuito corto, autogestión, reciclaje, soberanía.<br><br>
- Reflexionar sobre la brecha tecnológica y la brecha generacional.<br><br>
</blockquote>

* Enlace al vídeo en <a href="https://w.wiki/4Jrh" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre_2021_P25_-_Manu,_Raquel_-_Competencias_digitales_con_mirada_cr%C3%ADtica.webm?embedplayer=yes" width="640" height="360" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h2 id="demanda-convenios-google">11:50-12:15 "Demanda judicial contra los convenios con Google, la necesidad de la acción colectiva"</h2>

**Luis Fajardo López**, Prof. de Derecho civil en la ULL. Defensor de las libertades en el ciberespacio. Dr. por la UAM, ha dado clases en la UAM, UAL, UdG, y UNED. Ha sido abogado, Juez, y DPD. Actualmente está centrado en su trabajo docente y de investigación en la ULL y en la constitución de la Fundación Tecnología para la Sociedad, txs.es.

* **Web del proyecto**: <https://www.educar.encanarias.info/fundacion/>
* **Web personal**: <https://encanarias.info/people/56066f30d1620135a1394b44294f2d2e>
* **Mastodon**: [@lfajardo@txs.es](https://txs.es/@lfajardo)
* **Twitter**: [@lfajardo](https://twitter.com/lfajardo)

<blockquote>
Al principio de la pandemia reaccioné como padre, abogado y DPD: el convenio con Google de mi comunidad autónoma es ilegal (como los demás), y serviría además de ariete para que Google entrase en otras comunidades. Me indignaba que incapaz la Administración de decidir sobre la legalidad de sus actuaciones, pudiese la autorización a los padres. Pedí el expediente, lo recurrí ante la Administración, pero para la vía judicial no podía presentarlo solo... Te contaré los argumentos, lo que ha venido después preparando Tecnologías para la Sociedad, y cómo puedes colaborar para presentarla antes del comienzo de curso en todos aquellos lugares en los que exista el problema.<br><br>
Hace ya más de un año que nos vimos obligados, llevados por la pandemia, a hacer frente al desembarco masivo de tecnologías que invaden la privacidad de nuestros hogares. Como profesor monté un #Jitsi para dar mis clases, colaboré más que me enfrenté a las políticas de mi universidad, y junto a otros compañeros terminamos montando el embrión de #EDUCATIC, que hoy gestiona la plataforma de videoconferencias libre (BigBlueButton) con la que impartimos docencia, con interés de los compañeros (impartimos un curso a profesorado al que se inscribieron 179) y el beneplácito del Rectorado. Seguimos avanzando porque queda mucho por hacer.<br><br>
Finalmente constituimos la Asociación EDUCATIC, Tecnologías para la Educación y Transparencia Tecnológica, una de cuyas finalidades es dar vida a una fundación que garantice la continuidad del proyecto: txs.es, correo libre y seguro, redes sociales libres y seguras, herramientas para el trabajo que respeten nuestros valores (y nuestras leyes, claro), y cómo no, aplicaciones para la docencia. Queremos seguir trabajando para que los centros educativos en España utilicen tecnología ética, que enseñen a nuestra juventud el potencial de la tecnología, sin hacerlos usuarios cautivos de plataformas de captación de datos y consumo tecnológico.<br><br>
Ahí entran las acciones judiciales, que detallaré. Puedes ver más en <a href="https://encanarias.info/posts/19db429025a40139409b26860af9fe8c" target="_blank">esta entrada</a>.
</blockquote>

* Enlace al vídeo en <a href="https://w.wiki/4Jri" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre_2021_P26_-_Luis_Fajardo_-_Demanda_judicial_contra_los_convenios_con_Google,_la_necesidad_de_la_acci%C3%B3n_colectiva.webm?embedplayer=yes" width="640" height="360" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h2 id="propuesta-fase-cgt">12:30-12:55 "En defensa de una educación digital pública: la propuesta de FASE CGT"</h2>

**Juan Miguel Mendoza Garrido**, profesor EESS y Secretario de Organización de FASE CGT.

* **Web de la iniciativa**: <https://edigpubfasecgt.org/>
* **Correo electrónico**: <sopri@edigpub.org>

<blockquote>
Ante el proceso acelerado de privatización de los recursos digitales de la educación pública en Andalucía, FASE CGT presenta a la comunidad educativa un manifiesto, abierto a ampliación, en el que se señalan los pilares fundamentales que creemos que deben sostener el entramado digital de la escuela pública, y por los que debe luchar toda la comunidad educativa en aras de garantizar su soberanía y privacidad digitales.<br><br>
La privatización avanza a golpe de convenios con multinacionales como Google o Microsoft, que con la excusa del “coste cero” pueden terminar haciéndose con el monopolio no solo de las plataformas digitales de la escuela pública, sino también con el control de sus contenidos educativos y de los datos personales de todos los miembros de la comunidad educativa.<br><br>
La supresión el IEDA (Instituto de Enseñanzas a Distancia de Andalucía) y la dispersión de sus enseñanzas y cometidos, es un ejemplo de esta política de desmantelar los recursos y medios públicos de la educación digital para en el corto plazo justificar la necesidad de su privatización.<br><br>
FASE CGT pone al servicio de la comunidad educativa una web en permanente actualización para difundir noticias y conocimiento que fomenten el uso de los recursos abiertos y de softward libre en los centros públicos, así como apoyo y asistencia a quienes se sientan compelidos a renunciar a lo público para someterse a la dictadura de las corporaciones.<br><br>
</blockquote>

* Enlace al vídeo en <a href="https://w.wiki/4Jrj" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre_2021_P27_-_Juan_Miguel_Mendoza_-_En_defensa_de_una_educaci%C3%B3n_digital_p%C3%BAblica,_la_propuesta_de_FASE_CGT.webm?embedplayer=yes" width="640" height="360" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h2 id="hezkuntzan-librezale">12:55-13:20 "Hezkuntzan librezale"</h2>

**Hezkuntzan ere Librezale** fue creado en 2017 por padres, profesores, alumos y agentes de Euskal Herria, preocupados por la creciente presencia de grandes empresas privadas en los procesos de digitalización de nuestras escuelas. En la actualidad, formamos este grupo más de 200 personas de toda Euskal Herria, tanto de la red pública como de la concertada.

* **Web del colectivo**: <https://hezkuntza.librezale.eus/>

<blockquote>
Teniendo en cuenta la situación de la educación vasca, desde el grupo Hezkuntzan ere Librezale queremos presentar la siguiente declaración para expresar nuestras inquietudes:<br><br>
1. La cesión de los datos de nuestros alumnos a empresas privadas impide garantizar su privacidad. El hecho de que entidades públicas impulsen que nuestros alumnos se conviertan en productos tiene graves implicaciones.<br><br>
2. Invertir en dispositivos Chromebook que solo garantizan cinco años de vida es una irresponsabilidad, ya que la obsolescencia programada va en contra de la reutilización.<br><br>
3. Dejar la presencia del euskera en los dispositivos que utilizarán nuestros alumnos a merced de los intereses y directrices de las empresas privadas es una decisión contraria a nuestra lengua. Es más, en los dispositivos Chromebook, hoy en día, hay varios apartados que no están en euskera.<br><br>
4. Restringir una y otra vez al alumnado el uso de las mismas herramientas es reducir la educación digital. La competencia digital, según las leyes educativas de Europa y del País Vasco, es una competencia básica a alcanzar por el alumnado, y limitarles a estas herramientas no la garantiza en ningún caso.<br><br>
5. El riesgo de crear dependencia de estas herramientas en la comunidad educativa es un grave problema. Impulsados por las instituciones, estamos creando potenciales usuarios y clientes de productos de empresas privadas.
</blockquote>

* Enlace al vídeo en <a href="https://w.wiki/4Jrk" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre_2021_P28_-_Aimar_-_Hezkuntzan_Librezale.webm?embedplayer=yes" width="640" height="360" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h2 id="educamadrid-plataforma-educativa-libre">13:20-13:45 "EducaMadrid, la Plataforma Tecnológica Educativa y Libre de la Comunidad de Madrid"</h2>

**Ezequiel Cabrillo**. Físico de cálculo automático, profesor de Tecnología en Secundaria y actualmente en el Ministerio de Cultura y Deporte dando soporte principalmente a protección de datos. Personalmente, defensor del uso del software libre en las aulas y en la administración pública. Miembro del grupo de desarrollo de Madrid-linuX y entusiasta de EducaMadrid. Creador del grupo de Telegram de apoyo a EducaMadrid "Teletrabajo docente EducaMadrid".

* **Web del proyecto**: <https://www.educa2.madrid.org/educamadrid/>
* **GitHub**: <https://github.com/maxezek>
* **Twitter**: [@maxezek1](https://twitter.com/maxezek1)

<blockquote>
EducaMadrid es una Plataforma Tecnológica Educativa basada en Software Libre que ofrece múltiples servicios interconectados y complementarios: webs de los centro, blogs, aulas virtuales, mediateca, cloud, correo, videoconferencias, MAdrid_linuX, etc.<br><br>
Ofrece la posibilidad de desarrollar la competencia digital, la comunicación entre alumnado y docentes e implementar procesos de enseñanza aprendizaje tanto de manera presencial como a distancia.<br><br>
Es un entorno seguro que cumple con la normativa sobre Protección de Datos sin generar huella digital a sus usuarios.<br><br>
Es un entorno libre que respeta la autoría de los contenidos generados por sus usuarios y que está alojado en servidores propios situados en la Comunidad de Madrid.<br><br>
Es un entorno sostenible que no genera usuarios cautivos, que aporta riqueza a nuestro entorno contratando empresas locales y que facilita la movilidad entre centros y la colaboración entre sus usuarios.
</blockquote>

* Enlace al vídeo en <a href="https://w.wiki/4Jrm" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre_2021_P29_-_Ezequiel_Cabrillo_-_EducaMadrid,_la_plataforma_tecnol%C3%B3gica_educativa_de_la_Comunidad_de_Madrid.webm?embedplayer=yes" width="640" height="360" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h2 id="mesa-redonda">16:00-16:50 "Mesa redonda: Soberanía Digital en las Aulas (Fuera Google)"</h2>

Espacio abierto para el debate sobre todo lo comentado en las intervenciones de la mañana. Primero se hará un repaso sobre todo lo comentado en la mañana y quien quiera (sea alguien que haya intervenido en la sesión de la mañana o no) podrá participar libremente en el debate para comentar su punto de vista sobre lo aportado durante la jornada, así como lanzar propuestas propias.

<h2 style="text-align:center">BLOQUE B: "Derechos Digitales y Privacidad en Internet"</h2>

<h2 id="redes-sociales-transicion-social">16:50-17:15 "Redes sociales para una transición social"</h2>

**Niko** es admin de los servicios de <a href="https://www.nogafam.es/">nogafam.es</a>. Empezó siendo desarrollador de software y terminó en el lado oscuro (ingeniero de sistemas) de una empresa de Bilbao dedicada al open source. En <a href="https://blog.nogafam.es/blog/como-surgio-la-idea-del-proyecto-nogafam" target="_blank">este artículo</a> da un mayor detalle de cómo surgió el proyecto y su propuesta para solucionar el problema. Ha realizado algunos podcasts en colaboración de otra gente del Fediverso y también ha formado parte de charlas online sobre la soberania digital.

* **Web del proyecto**: <https://www.nogafam.es/>
* **Mastodon**: [@admin@masto.nogafam.es](https://masto.nogafam.es/@admin)

<blockquote>
Las redes sociales libres y federadas (el Fediverso) tienen un potencial increible que se debe dar a conocer a las personas. Me encantaría poder hablar de eso!<br><br>

Si bien el software libre no está adoptado de forma masiva, hay un campo en el que me atrevería a decir que no se usa ni un 5%: las redes sociales.<br><br>

El Fediverso es un mundo de redes sociales federadas que buscan un estandar en este campo, y viene para quedarse como el e-mail. Es solo cuestión de tiempo, de conocimiento y de mejora que esto acabe siendo así.
</blockquote>

* Enlace al vídeo en <a href="https://w.wiki/4Jrn" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre_2021_P30_-_Niko_-_Redes_sociales_libres,_descentralizadas_y_est%C3%A1ndar.webm?embedplayer=yes" width="640" height="360" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h2 id="libertar-para-compartir">17:30-17:55 "Libertad para compartir"</h2>

**Dario Castañé** es ingenierio informático, activista político y contribuidor en software libre. Actualmente dedica su (escaso) tiempo libre a proveer de herramientas de voto a organizaciones sin ánimo de lucro, mantener [Mergo](https://github.com/imdario/mergo) (una librería que vive en el corazón del cloud, usada por Docker, Kubernetes, etc.) desarrollada en Go y buscar nuevos campos en los que trabajar por un impacto social positivo usando la tecnología.

* **Web del proyecto**: <https://freesharing.eu/es>
* **Web personal**: <https://da.rio.hn>
* **GitHub**: <https://github.com/imdario/>
* **Mastodon**: [@dario@mastodon.social](https://mastodon.social/@dario)
* **Twitter**: [@darccio](https://twitter.com/darccio)

<blockquote>
La Iniciativa Ciudadana Europea Freedom to Share aboga por la adopción de una disposición legislativa que contemple una exención de los derechos de autor, derechos afines y derechos sui generis del fabricante de bases de datos para las personas físicas que comparten archivos a través de redes digitales para fines personales y sin ánimo de lucro.
</blockquote>

* Enlace al vídeo en <a href="https://w.wiki/4Jro" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre_2021_P31_-_Dario_Casta%C3%B1%C3%A9_-_Libertad_para_compartir.webm?embedplayer=yes" width="640" height="360" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h2 id="politizar-la-tecnologia">17:55-18:20 "Politizar la tecnología: radios comunitarias y derecho a la comunicación en los territorios digitales"</h2>

**Inés Binder** es comunicadora social. Investiga temas de feminismo, tecnologías y políticas de comunicación. Es cofundadora del [Centro de Producciones Radiofónicas (CPR)](https://cpr.org.ar/) y parte del espacio hackfeminista [la_bekka](https://labekka.red/).

**Santiago García Gago** es comunicador social. Investiga temas de tecnopolítica y medios comunitarios. Es parte de [RadiosLibres.net](https://radioslibres.net/), [Radialistas.net](https://radialistas.net/) y de la [Red de Radios Comunitarias y Software Libre](https://liberaturadio.org/).

* **Web del proyecto**: <https://radioslibres.net/politizar-la-tecnologia/>
* **Mastodon**: [@radioslibres@mastodon.social](https://mastodon.social/@radioslibres)
* **Twitter**: [@RadiosLibres](https://twitter.com/radioslibres)

<blockquote>
Hoy en día la defensa del derecho a la comunicación exige, necesariamente, la apuesta por la soberanía tecnológica. Los medios libres, alternativos y comunitarios tienen experiencia en subvertir una tecnología unidereccional como la radio para garantizar la participación de las audiencias. En esta charla nos preguntaremos cuál es el nuevo horizonte tecnológico en la defensa del derecho a la comunicación desde los medios libres y esbozaremos algunas respuestas.<br><br>

Las radios comunitarias siempre han acompañado la defensa de estos territorios y de sus bienes comunes. Desde sus micrófonos se desenmascararon las falsas promesas de las mineras y se denunció cómo su explotación contaminaba los ríos. Apoyaron la organización vecinal que luchaba por una vida digna en los barrios periféricos de las grandes ciudades. Amplificaron los reclamos de los campesinos frente al ecocidio de los cultivos extensivos de palma africana. Y reclamaron un espectro radioeléctrico más diverso que reflejara la realidad y la cultura de sus comunidades.<br><br>

Hoy, estos territorios continúan siendo saqueados. Pero el asedio y la amenaza se extienden a los nuevos territorios digitales que habitamos. Entre las fronteras de Internet también existen fuerzas que colonizan y mercantilizan todos los aspectos de nuestra vida. Y de la misma manera, la ciudadanía resiste y se organiza alrededor de iniciativas que construyen autonomía y soberanía tecnológica.<br><br>

En esta charla plantearemos cómo podemos entender las iniciativas de soberanía tecnológica desde el marco de la defensa del derecho a la comunicación, con el que vienen trabajando los medios libres y comunitarios desde hace décadas.
</blockquote>

* Enlace al vídeo en <a href="https://w.wiki/4Jro" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre_2021_P32_-_In%C3%A9s_Binder,_Santiago_Garc%C3%ADa_Gago_-_Politizar_la_tecnolog%C3%ADa,_radios_comunitarias_y_derecho_a_la_comunicaci%C3%B3n_en_los_territorios_digitales.webm?embedplayer=yes" width="640" height="360" frameborder="0" style="display:block; margin: 0 auto"></iframe>

<h2 id="software-libre-administracion-publica">18:20-18:45 "Software Libre en la Administración Pública: lo que pudo ser y no fue"</h2>

**Lorena Sánchez**. Investigadora doctoral en Human-Computer Interaction en la Universidad de Luxemburgo. Jurista y politóloga por la Universidad Carlos III de Madrid. Master en Media and Communications Governance por la London School of Economics. Internet Society Youth Ambassador Fellow 2020. Ha centrado su carrera profesional en la privacidad y seguridad de la información, aunque está especializada en la interesección entre tecnología y ciencias sociales, plataformas y gobernanza en internet. Activista. Investiga y colabora activamente con distintas organizaciones como [Internet Society](https://www.isoc-es.org/), [Polikracia](https://polikracia.com/) e **Interferencias**.

* **Twitter**: [@LastStrawberry](https://twitter.com/LastStrawberry)

<blockquote>
El propósito de la presente charla es explicar el estado del software libre en la Administración Pública, entender cuál es el status jurídico de la implementación de software libre, así como ver cuál es nivel real de penetración de software libre en la administración pública española. Se presentará un análisis comparado con otros Estados.<br><br>

Además, se hablará de las principales consecuencias que tiene para los derechos y libertades de los administrados, y casos actuales en los que el uso de software privativo está siendo un problema para la ciudadanía.
</blockquote>

* Enlace al vídeo en <a href="https://w.wiki/4Jrq" target="_blank">Wikimedia Commons</a>.

<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://commons.wikimedia.org/wiki/File:EsLibre_2021_P33_-_Lorena_S%C3%A1nchez_-_Software_Libre_en_la_Administraci%C3%B3n_P%C3%BAblica,_lo_que_pudo_ser_y_no_fue.webm?embedplayer=yes" width="640" height="360" frameborder="0" style="display:block; margin: 0 auto"></iframe>
