---
layout: post
title:  DeepSec 2018&#58 Open Source Network Monitoring
author: germaaan
image:
  feature: banners/header.jpg
tags: charlas
---

**DeepSec** es un congreso internacional sobre seguridad en sistemas informáticos y comunicaciones electrónicas, así como la gestión de la seguridad y los aspectos sociales. Se celebra anualmente en Viena.

Este año, Paula participó con la charla **"Open Source Network Monitoring"**:

<blockquote>
<p>
I'd like to offer an introduction into Network System Monitoring using different open tools available in linux. The talk is a technical approach to identify the best sniffing points in a network and how to orchestrate a full analysis of the content to secure the network, as well as showing ideas of collaborative and distributed hacking.
</p>
<p>
Also, for a better performance, the talk includes a brief guide into configuring a Raspberry PI for creating a simple Network Capture Probe.
</p>
<p>
The main point of the talk is to show how open source tools are a nice option for this kind of security assessment.
</p>
</blockquote>

<iframe src="https://player.vimeo.com/video/308500968" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
