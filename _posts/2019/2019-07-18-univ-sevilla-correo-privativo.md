---
layout: post
title: La Universidad de Sevilla migra servicio correo electrónico a un sistema privativo
author: nacheteam
image:
  feature: banners/header.jpg
tags: noticias
---

Contaba en Twitter David Benavides (profesor en Escuela Técnica Superior de Ingeniería Informática de la Universidad de Sevilla) que la Universidad de Sevilla ha decidido migrar su servicio de correo electrónico propio basado en software libre a uno de Microsoft.

Es una mala noticia por la pérdida de autonomía que representa. El camino debería ser al contrario, de lo privativo hacia el software libre.

Podéis encontrar el hilo original aquí: [https://twitter.com/davbencue/status/1151806699612909569](https://twitter.com/davbencue/status/1151806699612909569).
