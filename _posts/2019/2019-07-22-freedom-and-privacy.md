---
layout: post
title: On freedom and privacy 
author: terceranexus6
image:
  feature: banners/header.jpg
tags: privacidad debate
---

## Freedom

Freedom of the software implies so many things. Maybe the will of enrolling freedom of the software is offering a fairer future for the next generations, allow the tech to spread further, or just because it just feels like the right thing to do. But, **where are we using this freedom?**

## Privacy

Internet is made out of information: users’ digital footprint, communication, posts and more. Are we facing information sovereignty or a products based structure¹? Think about all the personal information that is sold and collected on social networks and other services. Freedom of the software implies believing in equality and transparency, does it engage with data trading? The reality of digital surveillance is well known but widely ignored, as it’s uncomfortable and difficult to handle. Does it make sense that many companies based on data trading are the standard-bearer of freedom of the software? Is Freedom of the Software another way of showing off in these cases? I actually [wrote a bit about that here already](({{ site.url }}/2018/11/15/the-point-freedom/), and thought about all your answers. There’s this interesting speech called [“Linux sucks. Forever”](https://invidio.us/watch?v=TVHcdgrqbHE). I know the name is a trigger but trust me on this one, it’s a very good speech, which made me think of this (even tho it’s not exactly about this but about the path Linux is taking right now) and for some months I’ve been trying to better understand freedom of the software communities and motivation. Is that “rightness” feeling enough to keep Freedom of the software standing still, or evolving in society?

Not so long ago I was invited to a [KDE event](https://www.muylinux.com/2019/05/17/akademy-es-2019/) and asked about my job and related, and then an interesting question emerged “why is freedom of the software necessary for privacy?” My main point at that time was transparency (and, due to my job tendencies, security), but I kept thinking about it, and that concept evolved into trust. Our current digital context could be easily described as “unfair”. Data trading based companies know a lot about us, but we don’t really have their transparency back², which causes an uncomfortable distrust, accepted though. In this context, privacy is a claim for equality. But then, is privacy needed if trust existed in this relationship between services and users? When we ask for privacy we ask for a basic right, therefore a basic claim in a fair society, something that should be there and ideally should be granted.

##  To sum up

In conclusion, digital privacy is a right and freedom of the software a needed property of it, which makes freedom of the software more of a philanthropist cause, more than the rightness, but a praxis for a fair internet.

I encourage you guys to give your opinions on these thoughts and to discuss if we should be thinking about reasons why freedom of the software is in fact needed and not just optional.


*Also written in: [https://dev.to/terceranexus6/on-freedom-and-privacy-3612)*
