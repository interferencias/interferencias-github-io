---
layout: post
title: Noticias semana 22 al 28 de julio de 2019
author: germaaan
image:
  feature: banners/header.jpg
tags: noticias
---

Estas son las noticias más relevantes que hemos encontrado en nuestras redes sociales esta semana:

- **Qué es el "oscuro" capitalismo de la vigilancia de Facebook y Google y por qué lo comparan con la conquista española**: [https://www.bbc.com/mundo/noticias-47372336](https://www.bbc.com/mundo/noticias-47372336)
- **El filtro burbuja: ¿‘Un mundo feliz’?**: [https://ctxt.es/es/20170712/Politica/13847/filtro-burbuja-internet-datos-facebook-algoritmos.htm](https://ctxt.es/es/20170712/Politica/13847/filtro-burbuja-internet-datos-facebook-algoritmos.htm)
- **Don’t Let Encrypted Messaging Become a Hollow Promise**: [https://www.eff.org/deeplinks/2019/07/dont-let-encrypted-messaging-become-hollow-promise](https://www.eff.org/deeplinks/2019/07/dont-let-encrypted-messaging-become-hollow-promise)
- **Hackers roban documentos que apuntan que Rusia quiere que TOR deje de ser anónimo y está recopilando datos en las redes sociales**: [https://www.genbeta.com/actualidad/hackers-roban-documentos-que-apuntan-que-rusia-quiere-que-tor-deje-ser-anonimo-esta-recopilando-datos-redes-sociales](https://www.genbeta.com/actualidad/hackers-roban-documentos-que-apuntan-que-rusia-quiere-que-tor-deje-ser-anonimo-esta-recopilando-datos-redes-sociales)
- **Israel to arm students on autism spectrum with cybersecurity skills**: [https://www.timesofisrael.com/israel-to-arm-students-on-autism-spectrum-with-cybersecurity-skills/](https://www.timesofisrael.com/israel-to-arm-students-on-autism-spectrum-with-cybersecurity-skills/)
- **Thank Q, Next**: [https://www.eff.org/deeplinks/2019/07/thank-q-next](https://www.eff.org/deeplinks/2019/07/thank-q-next)
- **El anonimato va camino de desaparecer**: [https://elpais.com/tecnologia/2019/07/24/actualidad/1563927638_772353.html](https://elpais.com/tecnologia/2019/07/24/actualidad/1563927638_772353.html)
- **Office 365 declared illegal for German schools**: [https://tutanota.com/blog/posts/microsoft-office-365-email-alternative/](https://tutanota.com/blog/posts/microsoft-office-365-email-alternative/)
- **“Cedemos los datos sin recibir nada a cambio. Debería haber un mercado”**: [https://elpais.com/tecnologia/2019/07/24/actualidad/1563979384_129217.html](https://elpais.com/tecnologia/2019/07/24/actualidad/1563979384_129217.html)
- **GitHub blocked my account and they think I’m developing nuclear weapons**: [https://medium.com/@hamed/github-blocked-my-account-and-they-think-im-developing-nuclear-weapons-e7e1fe62cb74](https://medium.com/@hamed/github-blocked-my-account-and-they-think-im-developing-nuclear-weapons-e7e1fe62cb74)
- **VLC no es vulnerable y no tienes que desinstalarlo**: [https://unaaldia.hispasec.com/2019/07/vlc-no-es-vulnerable-y-no-tienes-que-desinstalarlo.html](https://unaaldia.hispasec.com/2019/07/vlc-no-es-vulnerable-y-no-tienes-que-desinstalarlo.html)
- **GitHub starts blocking developers in countries facing US trade sanctions**: [https://www.zdnet.com/article/github-starts-blocking-developers-in-countries-facing-us-trade-sanctions/](https://www.zdnet.com/article/github-starts-blocking-developers-in-countries-facing-us-trade-sanctions/)

Recuerda que además de poder seguirnos en [Twitter](https://twitter.com/Inter_ferencias) y [Mastodon](https://mastodon.technology/@interferencias), te invitamos a participar y comentar sobre todo esto en nuestro grupo de [Telegram](https://t.me/inter_ferencias).
