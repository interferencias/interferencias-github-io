---
layout: post
title: El problema de GitHub (y los sistemas centralizados cerrados)
author: germaaan
image:
  feature: banners/header.jpg
tags: noticias
---

[GitHub ha comenzado a bloquear las cuentas de los desarrolladores que se encuentran en países sancionados por EEUU](https://www.genbeta.com/actualidad/github-ha-comenzado-a-bloquear-cuentas-desarrolladores-que-se-encuentran-paises-sancionados-eeuu).

Nos entristece (pero no nos sorprende) que una plataforma que ha contribuido tanto al software libre vaya camino de ser una herramienta más de represión, en este caso bajo la excusa de leyes absurdas basadas en los intereses comerciales de unos pocos en lugar del progreso generalizado (algo que ya de por si no podría ir más en contra de cuestión filosófica detrás del movimiento de la cultura libre).

Pero es el peligro que corremos hoy en día, donde muchas veces parece que solo importa el beneficio económico y a corto plazo. En muchas ocasiones parece que en este MundoReal™ no hay lugar para la ética, donde si confiamos plenamente en sistemas cerrados que intentan generar una dependencia sistémica, nos arriesgamos a que en cualquier momento "sus dueños" cambien las reglas a la fuerza cuando estas dejen de beneficiarles (o no les benefician tanto como pudieran).

Es esto lo que tenemos que evitar, la dependencia de sistemas cerrados. Las herramientas cerradas al fin y al cabo son también eso mismo: herramientas. Siempre debemos tener presente que cuando usemos un medio que escapa de nuestro "control" no puede ser más que un medio eventual a usar cuando no quede otra alternativa, pero nunca lo debemos considerar un fin.
