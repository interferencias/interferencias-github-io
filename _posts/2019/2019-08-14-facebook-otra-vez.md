---
layout: post
title: Facebook, otra vez
author: germaaan
image:
  feature: banners/header.jpg
tags: noticias
---

[Facebook pagó para transcribir los chats de audio de sus usuarios](http://mouse.latercera.com/facebook-escuchaba-audio-usuarios/).

Es importante tener en mente el problema para la privacidad que representan estas noticias, pero lo verdaderamente inquietante es el cinismo con el que mienten de forma metódica todos los responsables de estas compañías a la hora de hablar sobre este tema

Zuckerberg afirmó que todo el tema de las escuchas de los dispositivos era poco más que una conspiración cuando era plenamente consciente de que en mayor o menor medida estaba mintiendo. Y podía decir esto con cierta tranquilidad porque sabía que no le iba a pasar nada ni a él ni a su compañía, el peor de los casos, una multa que represente poco más de una centésima parte de sus beneficios. Al fin y al cabo ellos lo único que están haciendo es rentabilizar algo que añoran muchos políticos desde hace tiempo: el control absoluto de la población con la excusa de nuestra seguridad.

[We want to limit use of e2e encryption, confirms UK minister](https://techcrunch.com/2017/06/05/we-want-to-limit-use-of-e2e-encryption-confirms-uk-minister/)

Lo que estamos viendo que en Australia se está usando para atacar algo tan importante como la libertad de prensa.

[Australia's anti-encryption laws being used to bypass journalist protections, expert says](https://www.theguardian.com/australia-news/2019/jul/08/australias-anti-encryption-laws-being-used-to-bypass-journalist-protections-expert-says)

Sueño de totalitarismo supremo cuyo mejor ejemplo es el #SCS de China, donde un "ciudadano de bien" podría tener ventajas como descuentos en la calefacción invernal y el transporte público o mejores condiciones a la hora de pedir un préstamo.

[China wants to track and grade each citizen’s actions — it’s in the testing phase](https://www.cnbc.com/2019/07/26/china-social-credit-system-still-in-testing-phase-amid-trials.html)

Pero volviendo al tema original, no es de extrañar que las grandes compañías se ponga de acuerdo para colaborar en crear plataformas que faciliten la transferencia de datos entre ellas.

[Mover nuestros datos de un ecosistema a otro libremente está más cerca: Apple se une al proyecto abierto Data Transfer junto a Google, Facebook y Microsoft](https://www.xataka.com/servicios/mover-nuestros-datos-ecosistema-a-otro-libremente-esta-cerca-apple-se-une-al-proyecto-abierto-data-transfer-a-google-facebook-microsoft)

Así el usuario tendrá la sensación de comodidad de no necesitar nada fuera de ese ecosistema, y mientras ellas pueden seguir aumentando su dominio en el campo de la información digital (para hacer con ella lo que quieran en el presente o lo que se les ocurra en el futuro). Algo que cada vez es más palpable:

[El dilema de cómo regular a los titanes digitales](https://elpais.com/economia/2019/08/10/actualidad/1565469638_196784.html)

Si estas megacorporaciones siguen aumentando su poder acabarán siendo otro lobby que ejercerá toda la presión posible para hacer reales sus intereses (como pasa con la industria armamentística en USA o el sector eléctrico en España). Sobre todo si el negocio a parte de lucrativo para las empresas, también lo pudiera ser para el político de turno.

[We Analysed the 527,350 Facebook Ads placed by the US Presidential Candidates. Here Are The Results.](https://medium.com/applied-data-science/56-070-165-facebook-ad-spend-of-us-presidential-candidates-broken-down-by-age-and-gender-2dcc32fe2c02)

Es por eso que debemos exigir a los políticos que tengan conciencia tecnológica de los problemas digitales de esta era y se comprometan a hacer leyes que protejan realmente nuestra privacidad dándole la importancia que verdaderamente tiene. Porque en un mundo tan globalizado de poco sirve plantear soluciones individuales a problemas colectivos: no usar Facebook no hace que sus decisiones no te puedan afectar en un futuro. Nuestro papel no puede ser el de meros espectadores, pero tampoco podemos tomar el camino fácil, solo nos queda la reivindicación.
