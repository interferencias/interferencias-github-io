---
layout: post
title: Noticias semana 19 al 25 de agosto de 2019
author: germaaan
image:
  feature: banners/header.jpg
tags: noticias
---

Estas son las noticias más relevantes que hemos encontrado en nuestras redes sociales esta semana:

- **What is surveillance capitalism and how does it shape our economy?”**:
[https://www.abc.net.au/news/2019-06-25/surveillance-capitalism-how-facebook-google-collect-your-data/11243362](https://www.abc.net.au/news/2019-06-25/surveillance-capitalism-how-facebook-google-collect-your-data/11243362)
- **Ekaitz Cancela: “Facebook es la viagra de un sistema financiero en plena crisis”**:
[https://www.elsaltodiario.com/tecnologia/ekaitz-cancela-facebook-gafa-capitalismo-digital-viagra-sistema-financiero-crisis](https://www.elsaltodiario.com/tecnologia/ekaitz-cancela-facebook-gafa-capitalismo-digital-viagra-sistema-financiero-crisis)
- **Cambridge Analytica whistleblower Chris Wylie says data still not safe from tech giants like Facebook**:
[https://www.abc.net.au/news/2019-08-09/chris-wylie-cambridge-analytica-data-tech-giants-facebook/11399552](https://www.abc.net.au/news/2019-08-09/chris-wylie-cambridge-analytica-data-tech-giants-facebook/11399552)
- **Riesgos digitales durante y después de una protesta**:
[https://infoactivismo.org/riesgos-digitales-durante-y-despues-una-protesta/](https://infoactivismo.org/riesgos-digitales-durante-y-despues-una-protesta/)
- **16 million Americans will vote on hackable paperless machines**:
[https://www.technologyreview.com/s/614148/16-million-americans-will-vote-on-hackable-paperless-voting-machines/](https://www.technologyreview.com/s/614148/16-million-americans-will-vote-on-hackable-paperless-voting-machines/)
- **La información personal que revelamos al compartir nuestro número telefónico**:
[https://www.nytimes.com/es/2019/08/16/espanol/ciencia-y-tecnologia/datos-telefono-informacion-seguridad.html](https://www.nytimes.com/es/2019/08/16/espanol/ciencia-y-tecnologia/datos-telefono-informacion-seguridad.html)
- **Blockchain y democracia digital: ¿descentralización o acto de fe?**:
[https://www.ticbeat.com/innovacion/blockchain-y-democracia-digital-descentralizacion-o-acto-de-fe/](https://www.ticbeat.com/innovacion/blockchain-y-democracia-digital-descentralizacion-o-acto-de-fe/)
- **Moda para hackear el control social: una diseñadora crea ropa que trolea a las cámaras de videovigilancia de tráfico**:
[https://www.eldiario.es/tecnologia/disenadora-hackear-camaras-videovigilancia-Unidos_0_932656917.html](https://www.eldiario.es/tecnologia/disenadora-hackear-camaras-videovigilancia-Unidos_0_932656917.html)
- **Twitter is displaying China-made ads attacking Hong Kong protesters**:
[https://www.engadget.com/2019/08/18/twitter-china-ads-attack-hong-kong-protesters](https://www.engadget.com/2019/08/18/twitter-china-ads-attack-hong-kong-protesters)
- **More debate about social media’s impact on society**:
[https://www.internetgovernance.org/2019/08/18/more-debate-about-social-medias-impact-on-society/](https://www.internetgovernance.org/2019/08/18/more-debate-about-social-medias-impact-on-society/)
- **Twitter is blocked in China, but its state news agency is buying promoted tweets to portray Hong Kong protestors as violent**:
[https://techcrunch.com/2019/08/19/twitter-is-blocked-in-china-but-its-state-news-agency-is-buying-promoted-tweets-to-portray-hong-kong-protestors-as-violent/](https://techcrunch.com/2019/08/19/twitter-is-blocked-in-china-but-its-state-news-agency-is-buying-promoted-tweets-to-portray-hong-kong-protestors-as-violent/)
- **Fuga de datos: el gran hermano de internet lo sabe todo sobre ti**:
[https://www.abc.es/tecnologia/abci-fuga-datos-gran-hermano-internet-sabe-todo-sobre-201908190235_noticia.html](https://www.abc.es/tecnologia/abci-fuga-datos-gran-hermano-internet-sabe-todo-sobre-201908190235_noticia.html)
- **Paging Big Brother: in Amazon’s bookstore, Orwell gets a rewrite**:
[https://www.nytimes.com/2019/08/19/technology/amazon-orwell-1984.html](https://www.nytimes.com/2019/08/19/technology/amazon-orwell-1984.html)
- **The Browser Monopoly**:
[http://blairreeves.me/2019/08/20/the-browser-monopoly/](http://blairreeves.me/2019/08/20/the-browser-monopoly/)
- **Moscow's blockchain voting system cracked a month before election**:
[https://www.zdnet.com/article/moscows-blockchain-voting-system-cracked-a-month-before-election/](https://www.zdnet.com/article/moscows-blockchain-voting-system-cracked-a-month-before-election/)
- **Google is tightening its grip on your website**:
[https://onezero.medium.com/google-is-tightening-its-iron-grip-on-your-website-27e06b3150e0](https://onezero.medium.com/google-is-tightening-its-iron-grip-on-your-website-27e06b3150e0)
- **Bernie Sanders y el reconocimiento facial: "Es el último ejemplo de tecnología orwelliana que viola nuestra privacidad"**:
[https://www.fayerwayer.com/2019/08/bernie-sanders-reconocimiento-facial/](https://www.fayerwayer.com/2019/08/bernie-sanders-reconocimiento-facial/)
- **Vulneran seguridad de sistema de votación ruso basado en blockchain**:
[https://www.tekcrispy.com/2019/08/20/seguridad-sistema-votacion-moscu-blockchain/](https://www.tekcrispy.com/2019/08/20/seguridad-sistema-votacion-moscu-blockchain/)
- **Mozilla takes action to protect users in Kazakhstan**:
[https://blog.mozilla.org/blog/2019/08/21/mozilla-takes-action-to-protect-users-in-kazakhstan/](https://blog.mozilla.org/blog/2019/08/21/mozilla-takes-action-to-protect-users-in-kazakhstan/)
- **Cómo el 'phishing' explota nuestro cerebro para engañarnos**:
[https://www.technologyreview.es/s/11359/como-el-phishing-explota-nuestro-cerebro-para-enganarnos](https://www.technologyreview.es/s/11359/como-el-phishing-explota-nuestro-cerebro-para-enganarnos)
- **Google y Amazon buscan los datos y los clientes insatisfechos de las aerolíneas**:
[https://retina.elpais.com/retina/2019/08/19/tendencias/1566213886_536421.html](https://retina.elpais.com/retina/2019/08/19/tendencias/1566213886_536421.html)
- **Libra, una moneda digital capaz de alterar las políticas económicas globales**:
[https://retina.elpais.com/retina/2019/08/20/innovacion/1566314250_571494.html](https://retina.elpais.com/retina/2019/08/20/innovacion/1566314250_571494.html)
- **Los reguladores antimonopolio de la Unión Europea investigan Libra, la moneda digital de Facebook**:
[https://www.genbeta.com/actualidad/reguladores-antimonopolio-union-europea-investigan-libra-moneda-digital-facebook](https://www.genbeta.com/actualidad/reguladores-antimonopolio-union-europea-investigan-libra-moneda-digital-facebook)
- **YouTube elimina por error una pelea entre robots por 'crueldad animal'**:
[https://www.genbeta.com/actualidad/youtube-elimina-error-pelea-robots-crueldad-animal](https://www.genbeta.com/actualidad/youtube-elimina-error-pelea-robots-crueldad-animal)
- **El uso de Uber, Tinder y Pokemon Go como canales de organización ciudadana**:
[https://www.pabloyglesias.com/organizacion-digital-manifestaciones/](https://www.pabloyglesias.com/organizacion-digital-manifestaciones/)
- **Researcher publishes second Steam zero day after getting banned on Valve's bug bounty program**:
[https://www.zdnet.com/article/researcher-publishes-second-steam-zero-day-after-getting-banned-on-valves-bug-bounty-program/](https://www.zdnet.com/article/researcher-publishes-second-steam-zero-day-after-getting-banned-on-valves-bug-bounty-program/)
- **Berners-Lee: “La Web ha perdido el rumbo: hay muchas cosas que han ido mal”**:
[https://retina.elpais.com/retina/2019/08/20/tendencias/1566294630_915353.html](https://retina.elpais.com/retina/2019/08/20/tendencias/1566294630_915353.html)
- **Logran romper el esquema de cifrado del sistema de votación ruso basado en blockchain**:
[https://www.genbeta.com/actualidad/logran-romper-esquema-cifrado-sistema-votacion-ruso-basado-blockchain](https://www.genbeta.com/actualidad/logran-romper-esquema-cifrado-sistema-votacion-ruso-basado-blockchain)
- **Apple contractors listened to 1,000 Siri recordings per shift, says former employee**:
[https://www.irishexaminer.com/breakingnews/ireland/apple-contractors-listened-to-1000-siri-recordings-per-shift-says-former-employee-945575.html](https://www.irishexaminer.com/breakingnews/ireland/apple-contractors-listened-to-1000-siri-recordings-per-shift-says-former-employee-945575.html)
- **Facebook y Twitter denuncian que China usó cuentas falsas para desinformar**:
[https://www.nytimes.com/es/2019/08/22/espanol/mundo/hong-kong-facebook-twitter.html](https://www.nytimes.com/es/2019/08/22/espanol/mundo/hong-kong-facebook-twitter.html)
- **Deconstructing Google’s excuses on tracking protection**:
[https://freedom-to-tinker.com/2019/08/23/deconstructing-googles-excuses-on-tracking-protection/](https://freedom-to-tinker.com/2019/08/23/deconstructing-googles-excuses-on-tracking-protection/)
- **I visited 47 sites. Hundreds of trackers followed me**:
[https://www.nytimes.com/interactive/2019/08/23/opinion/data-internet-privacy-tracking.html](https://www.nytimes.com/interactive/2019/08/23/opinion/data-internet-privacy-tracking.html)

### Blogs:
- **El software de Google es malware**:
[https://www.gnu.org/proprietary/malware-google.html](https://www.gnu.org/proprietary/malware-google.html)
- **New release: Tor 0.4.1.5**:
[https://blog.torproject.org/new-release-tor-0415](https://blog.torproject.org/new-release-tor-0415)
- **Hackers y simpatizantes del proyecto GNU se reunirán en Madrid**:
[https://victorhckinthefreeworld.com/2019/08/22/hackers-y-simpatizantes-del-proyecto-gnu-se-reuniran-en-madrid/](https://victorhckinthefreeworld.com/2019/08/22/hackers-y-simpatizantes-del-proyecto-gnu-se-reuniran-en-madrid)
- **Disroot cumplió 4 años**:
[https://disroot.org/es/blog/disroot_4_years_old](https://disroot.org/es/blog/disroot_4_years_old)

### Recursos:
- **CyberChef, una aplicación web para encriptación, codificación, compresión y análisis de datos**:
[https://github.com/gchq/CyberChef](https://github.com/gchq/CyberChef)
- **Awesome-Selfhosted, lista de servicios de red de software libre y aplicaciones web que se pueden alojar localmente**:
[https://github.com/Kickball/awesome-selfhosted](https://github.com/Kickball/awesome-selfhosted)

### Redes sociales:
- **Marta Peirano ([@minipetite](https://twitter.com/minipetite/))**: "Manifestantes en Hong Kong tiran abajo un poste de cámara equipada con reconocimiento facial. Usan máscaras y paraguas para no ser reconocidos.":
[https://twitter.com/minipetite/status/1165562545861726208](https://twitter.com/minipetite/status/1165562545861726208)

- Hilo de **Edward Snowden ([@Snowden](https://twitter.com/Snowden/))** en el que comenta por el décimo aniversario de Tails, que a pesar de que la red Tor esté en entredicho, al igual que hizo en 2013 para revelar el sistema de vigilancia masivo de la NSA, es el sistema que volvería a usar por la simple premisa de que lo considera un sistema "suficientemente seguro" (aunque teniendo una seria de consideraciones en cuenta):
[https://twitter.com/Snowden/status/1165297667490103302](https://twitter.com/Snowden/status/1165297667490103302)

Recuerda que además de poder seguirnos en [Twitter](https://twitter.com/Inter_ferencias) y [Mastodon](https://mastodon.technology/@interferencias), te invitamos a participar y comentar sobre todo esto en nuestro grupo de [Telegram](https://t.me/inter_ferencias).
