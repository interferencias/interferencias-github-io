---
layout: post
title: A Google (también) le da igual vender la privacidad de tus hijos
author: germaaan
image:
  feature: banners/header.jpg
tags: noticias
---

No será casualidad que en estos tiempos uno de los grandes objetivos de las megacorporaciones de datos sea hacer dependientes a las instituciones educativas. Ven mucho más claro que la mayoría de políticos que es un área primordial para la sociedad (🤑🤑)

**Google Classroom, escuela y pedagogía unidimensional**: [https://www.elsaltodiario.com/el-rumor-de-las-multitudes/google-classroom-escuela-y-pedagogia-unidimensional](https://www.elsaltodiario.com/el-rumor-de-las-multitudes/google-classroom-escuela-y-pedagogia-unidimensional)

Los ejemplos de intenciones están a la orden del día, todo vale a costa de conseguir beneficios aunque vaya en perjuicio de sectores sin capacidad de defensa (y moldeables).

**Multa millonaria a Google por violar la privacidad de los niños en YouTube**: [https://elpais.com/sociedad/2019/09/04/actualidad/1567605248_751405.html](https://elpais.com/sociedad/2019/09/04/actualidad/1567605248_751405.html)

Por eso debería cundir el ejemplo de estas prohibiciones; pero más allá de esto, que las propias instituciones educativas se hicieran responsables de sus propias soluciones tecnológicas fomentando el uso de propuestas libres y de código abierto.

**Prohíben usar en escuelas alemanas Office 365 (y las nubes de Apple y Google) por "problemas de privacidad"**: [https://www.genbeta.com/seguridad/prohiben-usar-escuelas-alemanas-office-365-nubes-apple-google-problemas-privacidad](https://www.genbeta.com/seguridad/prohiben-usar-escuelas-alemanas-office-365-nubes-apple-google-problemas-privacidad)
