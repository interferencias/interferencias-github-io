---
layout: post
title: Let's talk about Linux
author: terceranexus6
image:
  feature: banners/header.jpg
tags: linux libertad debate
---

I wanted to share this with you guys. What do you love the most about Linux and freedom of the software?

<img src="{{ site.url }}/assets/images/dev.to/200307/e21e81a54959d3a7.png" style="display: block; margin: 0 auto;">

*Also written in: [https://dev.to/terceranexus6/let-s-talk-about-linux-1iip](https://dev.to/terceranexus6/let-s-talk-about-linux-1iip)*
