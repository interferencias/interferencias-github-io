---
layout: post
title: Your friend and neighbour WiFi
author: terceranexus6
image:
  feature: banners/header.jpg
tags: seguridad wifi
---

<img src="{{ site.url }}/assets/images/dev.to/200329/4ss2jhso1bkxean4vpw1_ab.jpg" style="display: block; margin: 0 auto;">

Recently I've been reading about ways to help your neighbourhood with the COVID-19 crisis, here in Spain. But one of the tips they gave was quite unsecure, because it's based in the fact that everyone is nice: "Share your WiFi!" well, it is a wonderful idea to share your Internet connection if you know how to protect it and have different channels for you and your guests. On the other hand trusting on unknown open WiFis is also a bad idea. I want to show today why, using a Wifi pineapple.

The [original WiFi pineapple](https://shop.hak5.org/products/wifi-pineapple) is a redteam gadget designed by Hak5, an offensive security exclusively brand. There are also Raspberry PI versions, but I'll use the original one. I'm using a pocket size version.

<img src="{{ site.url }}/assets/images/dev.to/200329/4ss2jhso1bkxean4vpw1.jpg" style="display: block; margin: 0 auto;">

<img src="{{ site.url }}/assets/images/dev.to/200329/uxtmf8xne7yro1evzj32.jpg" style="display: block; margin: 0 auto;">

This gadget simulates a router and captures traffic of the ones who connect to it. It aims to assist professionals in MITM (man in the middle) attacks, which consist on steal data of a connection or impersonate someone.

<img src="{{ site.url }}/assets/images/dev.to/200329/mitm.jpg" style="display: block; margin: 0 auto;">

This gadget is a lot of fun I must confess. You can access to the information launching a local web interface, seeing the networks around you, the devices and all. So when you are an user, what you see is an open connection sometimes using a familiar name such as "Starbucks" or something similar, and you can use the Internet (as the **Pineaaple** is connected to a legit source of Internet connection) so you might not notice you are being watched! Side note, if you try this in a security conference, you might get pranked when trying to do this attack as you might encounter WiFi connections around you named "**idiot**" or "**gotcha**".

In any case be careful out there, and even though an open WiFi is a huge temptation, don't be gullible. And if, even though, you want to share your WiFi, create a Guest's WiFi and share a password for it with neighbours.

*Also written in: [https://dev.to/terceranexus6/your-friend-and-neighbour-wifi-257a](https://dev.to/terceranexus6/your-friend-and-neighbour-wifi-257a)*
