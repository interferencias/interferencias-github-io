---
layout: post
title: Publicados los vídeos de esLibre 2021
author: germaaan
image:
  feature: banners/header.jpg
tags: eventos privacidad seguridad
---

<img src="{{ site.url }}/assets/images/eslibre/2021/banner_anuncio.png" style="display: block; margin: 0 auto;">

<p>
Ya están disponibles los vídeos de nuestra sala "Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google)" de esta edición de <strong>es<span style="color:red">Libre</span></strong>: podéis encontrarlos junto con el resto de <a href="https://commons.wikimedia.org/wiki/Category:EsLibre_2021" target="_blank"> vídeos de esta edición en Wikimedia Commons</a>.
</p>

<p>
Aunque también podeis encontrarlos con todo detalle de información en la página correspondiente que creamos aquí, así que simplemente os dejamos un enlace allí para que podáis echarle un ojo.
</p>

<h3>➡️ <a href="{{ site.url }}/eslibre/2021">Sala esLibre 2021: Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google)</a></h3>
